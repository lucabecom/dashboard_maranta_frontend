import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ExternalDashboardRoutes } from './external-dashboard.routing';

import { SectorDashboardComponent } from './sector-dashboard/sector-dashboard.component';
import { ExternalNavBarComponent } from './external-nav-bar/external-nav-bar.component';
import { RequestDetailComponent } from './request-detail/request-detail.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ExternalDashboardRoutes),
        FormsModule,
        FormsModule,
        NgxSpinnerModule
    ],
    declarations: [
        SectorDashboardComponent,
        ExternalNavBarComponent,
        RequestDetailComponent
    ]
})

export class ExternalDashboardModule {}
