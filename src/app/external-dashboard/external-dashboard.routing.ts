import { Routes } from '@angular/router';

import { SectorDashboardComponent } from './sector-dashboard/sector-dashboard.component';
import {RequestDetailComponent} from './request-detail/request-detail.component';

export const ExternalDashboardRoutes: Routes = [{
    path: '',
    children: [ {
        path: 'settore',
        component: SectorDashboardComponent
    }]
}, {

    path: '',
    children: [ {
        path: 'request/:requestid',
        component: RequestDetailComponent
    }]
}];
