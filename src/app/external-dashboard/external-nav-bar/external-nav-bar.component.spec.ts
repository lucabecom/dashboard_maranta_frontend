import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalNavBarComponent } from './external-nav-bar.component';

describe('ExternalNavBarComponent', () => {
  let component: ExternalNavBarComponent;
  let fixture: ComponentFixture<ExternalNavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalNavBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
