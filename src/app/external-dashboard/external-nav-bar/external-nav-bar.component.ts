import {Component, OnInit, Input} from '@angular/core';
import {timer} from 'rxjs';
import {take} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {window} from 'rxjs/internal/operators/window';
import {environment} from '../../../environments/environment';



@Component({
    selector: 'app-external-nav-bar',
    templateUrl: './external-nav-bar.component.html',
    styleUrls: ['./external-nav-bar.component.css']
})
export class ExternalNavBarComponent implements OnInit {
    @Input() title: string;

    currentDate: string;

    constructor(private router: Router) {
    }

    ngOnInit() {

        timer(0, 60000).pipe(
            take(60000)).subscribe(x => {
            this.currentDate = this.getCurrentDate();
            console.log(this.currentDate);
        })


    }

    logout() {
        if (localStorage.getItem(environment.brandname + '_SessionId')) {
            console.log('logout');
            localStorage.removeItem(environment.brandname + '_SessionId');
            this.router.navigateByUrl('pages/login');
        }
    }

    refresh() {
        // @ts-ignore
        window.location.reload();
    }

    getCurrentDate() {
        const data = new Date();
        let set, gg, mm, aaaa, h, m, s;
        // Crea la tabella dei mesi
        const mesi = new Array();
        mesi[0] = 'Gennaio';
        mesi[1] = 'Febbraio';
        mesi[2] = 'Marzo';
        mesi[3] = 'Aprile';
        mesi[4] = 'Maggio';
        mesi[5] = 'Giugno';
        mesi[6] = 'Luglio';
        mesi[7] = 'Agosto';
        mesi[8] = 'Settembre';
        mesi[9] = 'Ottobre';
        mesi[10] = 'Novembre';
        mesi[11] = 'Dicembre';
        // Crea la tabella dei giorni della settimana
        const giorni = new Array();
        giorni[0] = 'Domenica';
        giorni[1] = 'Lunedì';
        giorni[2] = 'Martedì';
        giorni[3] = 'Mercoledì';
        giorni[4] = 'Giovedì';
        giorni[5] = 'Venerdì';
        giorni[6] = 'Sabato';
        // Estrae dalla tabella il giorno della settimana
        set = giorni[data.getDay()] + ' ';
        gg = data.getDate() + ' ';
        // Estrae dalla tabella il mese
        mm = mesi[data.getMonth()] + ' ';
        aaaa = data.getFullYear();
        h = data.getHours() + ':';
        m = data.getMinutes();

        if (parseInt(h) <= 9) h = '0' + h;
        if (parseInt(m) <= 9) m = '0' + m;

        s = data.getSeconds();
        return set + gg + mm + aaaa + ' - ' + h + m;
    }

}
