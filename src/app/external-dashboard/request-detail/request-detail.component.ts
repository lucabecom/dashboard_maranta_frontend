import {Component, OnInit} from '@angular/core';
import {ApiGetRequestDetailService} from '../../service/request/api-get-request-detail.service';
import {GenericApiServiceService} from '../../service/generic-api-service.service';
import {DownloadService} from '../../service/download/download.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService } from 'ngx-spinner';
import {Location} from '@angular/common';
import {environment} from '../../../environments/environment';
import {ApiLoginUtenteService} from '../../service/utente/api-login-utente.service';

const sector = environment.sector;

@Component({
    selector: 'app-request-detail',
    templateUrl: './request-detail.component.html',
    styleUrls: ['./request-detail.component.css']
})


export class RequestDetailComponent implements OnInit {
    Request: any;
    requestid: number;
    sector: string = sector;

    constructor(private ApiGetRequestDetail: ApiGetRequestDetailService,
                private genericApiService: GenericApiServiceService,
                private location: Location,
                private router: Router,
                private downloadService: DownloadService,
                private route: ActivatedRoute,
                private spinner: NgxSpinnerService,
                private utente: ApiLoginUtenteService) {


        if (!this.utente.getMySession()) {
            this.router.navigateByUrl('pages/login');
        }

    }

    ngOnInit() {


        this.route.params.subscribe(params => {
            this.requestid = +params['requestid'];
            this.spinner.show();
            this.apiGetRequestDetail(this.requestid);
        });


    }

        inserisciRispostaPreimpostataInMessaggio(risposta: string) {
        console.log('risposta preimpostata');
        this.Request.DataForm.Fields.forEach(function (field) {
            if (field.Title === 'Nuovo messaggio') {
                field.Value = risposta;
            }
        });
    }



    apiGetRequestDetail(requestid: number): void {
        this.ApiGetRequestDetail.getRequestDetail(requestid)
            .subscribe(
                result => {

                    this.Request = result;
                    console.log(this.Request);
                    this.spinner.hide();

                },
                error => console.log('Error :: ' + error)
            )
    }

    apiRequestDetailUpdate(requestid: number): void {
        this.ApiGetRequestDetail.setRequestDetail(requestid)
            .subscribe(
                result => {

                    this.Request = result;
                    console.log(this.Request);
                    this.spinner.hide();

                },
                error => console.log('Error :: ' + error)
            )
    }

    apiDownloadAllegato(item: any): any {
        this.downloadService.getDocumento(item.Id)
            .subscribe(
                result => {

                    const blob = new Blob([result]);
                    const link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = item.Titolo;
                    link.click();

                },
                error => console.log('Error :: ' + error)
            )
    }

    genericApiPost(url: string, post: any): void {
        this.genericApiService.postApiUrl(url, post)
            .subscribe(
                result => {

                    this.Request = result;
                    this.spinner.hide();
                    this.postSbumitCallback();

                },
                error => console.log('Error :: ' + error)
            )
    }

    Submit(action) {
        // myArray.find(x => x.id === '45').foo;
        console.log(action)
        if (action.PostRequest) {
            this.spinner.show();
            this.genericApiPost(action.PostRequest, this.Request);
        }
        if (action.RedirectTo) {
            if (action.RedirectTo === 'HistoryBack') {
                this.location.back();
            }
            else {
                window.location.href = action.RedirectTo;
            }

        }

      /*  const messaggio = this.Request.DataForm.Fields.find(x => x.Title === 'Nuovo messaggio').Value;
        console.log(messaggio);
        if (messaggio != '') {
            this.spinner.show();
            this.apiRequestDetailUpdate(this.Request);
        } */
    }

    postSbumitCallback() {
        setTimeout(() => {
            if (this.Request.LastMessage.RedirectTo) {
                if (this.Request.LastMessage.RedirectTo === 'HistoryBack') {
                    this.location.back();
                } else {
                    window.location.href = this.Request.LastMessage.RedirectTo.RedirectTo;
                }
            }
        }, 2000 );
    };

    downloadAllegato(item) {
       // window.location.href = ;
        this.apiDownloadAllegato(item);


    }

    getMapValue(obj, key) {
        if (obj.hasOwnProperty(key)) {
            return obj[key];
        }
        return null;
    }

}
