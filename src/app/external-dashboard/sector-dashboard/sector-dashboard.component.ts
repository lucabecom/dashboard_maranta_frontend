import {Component, OnInit, OnDestroy} from '@angular/core';
import {ApiGetSectorListService} from '../../service/sector/api-get-sector-list.service';
import {GenericApiServiceService} from '../../service/generic-api-service.service';
import {ApiLoginUtenteService} from '../../service/utente/api-login-utente.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {environment} from '../../../environments/environment';
import { interval } from 'rxjs/observable/interval';
import { timer } from 'rxjs/observable/timer';
import {Router} from '@angular/router';


const sector = environment.sector;

@Component({
    selector: 'app-sector-dashboard',
    templateUrl: './sector-dashboard.component.html',
    styleUrls: ['./sector-dashboard.component.css']
})
export class SectorDashboardComponent implements OnInit, OnDestroy {

    requests: any;
    threeFirst: any = [];
    requestsCopy: any;
    resetAllButton: boolean;
    sector: string = sector;
    filterActivated: any;
    Timer: any;

    constructor(private ApiGetSectorList: ApiGetSectorListService,
                private genericApiService: GenericApiServiceService,
                private router: Router,
                private spinner: NgxSpinnerService,
                private utente: ApiLoginUtenteService) {

        if (!this.utente.getMySession()) {
            this.router.navigateByUrl('pages/login');
        }
    }

    ngOnDestroy() {
        if (this.Timer) {
            this.Timer.unsubscribe();
        }
    }


    ngOnInit() {
        this.resetAllButton = false;
        this.spinner.show();
        const Timer = timer(0,60000);
        this.Timer = Timer.subscribe(val => {
            console.log(this.filterActivated);
            if (sessionStorage.getItem('SectorRequests')) {
                const requests = JSON.parse(sessionStorage.getItem('SectorRequests'));
                this.setSelectedFilter(requests);
                if (this.filterActivated.PostForChange) {
                    this.genericApiPost(this.filterActivated.PostForChange, requests);
                } else {
                    this.apiGetSectorlist(this.sector);
                }

            } else if (typeof this.filterActivated !== 'undefined' && this.filterActivated.PostForChange) {
                this.genericApiPost(this.filterActivated.PostForChange, this.requests);

            } else {
                this.apiGetSectorlist(this.sector);
            }

        } );
    }

    sceltaFiltro(filter, option) {

        this.spinner.show();
        this.threeFirst = [];
        filter.FiltroSelezionato = option.Titolo;
        filter.Relations.forEach(function(item) {
            if (item.Id !== option.Titolo) {
                item.Selezionato = 'false';
            } else {
                item.Selezionato = 'true';

            }
        }.bind(this));

        option.Selezionato = true;
        this.resetAllButton = true;
        sessionStorage.setItem('SectorRequests',JSON.stringify(this.requests));
        this.genericApiPost(filter.PostForChange, this.requests);

    }

    rimuoviFiltro(filter) {

        this.spinner.show();
        this.threeFirst = [];
        filter.FiltroSelezionato = '';
        filter.Relations.forEach(function (option) {
            option.Selezionato = false;
        });
        this.resetAllButton = false;
        sessionStorage.setItem('SectorRequests',JSON.stringify(this.requests));
        this.genericApiPost(filter.PostForChange, this.requests);
    }

    generateAction(action) {
        this.spinner.show();
        if (action.Title = 'Reset') {
            sessionStorage.removeItem('SectorRequests');
        }
        this.genericApiPost(action.PostRequest, this.requests);
    }

    goToRequest(id) {
        this.spinner.show();
        sessionStorage.setItem('SectorRequests',JSON.stringify(this.requests));
        this.router.navigateByUrl('/external-dashboard/request/' + id);

    }

    apiGetSectorlist(sector: string): void {
        this.ApiGetSectorList.getSectorList(sector)
            .subscribe(
                result => {

                    this.requests = result;
                    this.blinkFirstIfUpdated();
                    this.setSelectedFilter(this.requests);
                    this.spinner.hide();
                },
                error => console.log('Error :: ' + error)
            )
    }

    apiGetSectorlistUpdate(sector: string): void {
        this.ApiGetSectorList.getSectorList(sector)
            .subscribe(
                result => {

                    this.requestsCopy = result;
                    this.setSelectedFilter(this.requests);
                    this.spinner.hide();
                },
                error => console.log('Error :: ' + error)
            )
    }

    genericApiPost(url: string, post: any): void {
        this.genericApiService.postApiUrl(url, post)
            .subscribe(
                result => {

                    this.requests = result;
                    this.blinkFirstIfUpdated();
                    this.setSelectedFilter(this.requests);
                    this.spinner.hide();

                },
                error => console.log('Error :: ' + error)
            )
    }

    setSelectedFilter(requests) {
        this.filterActivated = false;
        requests.DataForm.Fields.forEach(function (field) {
            field.Relations.forEach(function (option) {

                if (option.Selezionato == true) {
                    field.FiltroSelezionato = option.Titolo;
                    this.resetAllButton = true;
                    this.filterActivated = field;
                }
            }.bind(this));
        }.bind(this));
    }

    blinkFirstIfUpdated() {
        console.log(this.requests.CrudList.Rows);

        if(this.threeFirst.length === 0) {
            this.threeFirst.push(this.requests.CrudList.Rows[0]);
        } else {
            if (this.requests.CrudList.Rows[0].Id !== this.threeFirst[0].Id) {
                this.threeFirst[0] = this.requests.CrudList.Rows[0];
                this.requests.CrudList.Rows[0].Blink = true;
            } else {
                this.requests.CrudList.Rows[0].Blink = false;
            }
        }
    }


}
