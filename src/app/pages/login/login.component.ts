import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { environment } from '../../../environments/environment';
import {ApiLoginUtenteService} from '../../service/utente/api-login-utente.service';

declare var $: any;
const projectName = environment.name;


@Component({
    moduleId: module.id,
    selector: 'login-cmp',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})

export class LoginComponent implements OnInit{

    focus;
    focus1;
    focus2;

    utente: any;
    response: any;
    test: Date = new Date();
    username: string;
    password: string;
    projectName = projectName;
    alert: any = false;



    constructor(private ApiLoginUtente: ApiLoginUtenteService,
                private router: Router) {

    }

    ngOnInit() {
        if (localStorage.getItem(environment.brandname + '_SessionId')) {
            this.apiGetSession();
        }
    }

    keyDownFunction(event) {
        if (event.keyCode === 13) {
            this.login();
        }
    }

    login() {
        this.alert = false;
        this.apiLogin(this.username, this.password);
    }

    apiLogin(username: string, password: string): void {
        this.ApiLoginUtente.login(username, password)
            .subscribe(
                result => {
                    this.response = result;
                    if (result.SessionId) {
                        this.apiGetSession();

                    } else {
                        this.alert = true;
                    }},
                error => {
                    console.log('Error :: ' + error);
                    this.alert = true;
                }
            )
    }

        apiGetSession(): void {
        this.ApiLoginUtente.getMyId()
            .subscribe(
                result => {

                    this.response = result;
                    console.log(result);
                    if (result && result.Id) {
                        this.router.navigateByUrl('external-dashboard/settore');
                    }
                },
                error => console.log('Error :: ' + error)
            )
    }



}
