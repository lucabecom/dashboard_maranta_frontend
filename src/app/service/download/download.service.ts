import { Injectable } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, retry, map } from 'rxjs/operators';

const brandName = environment.brandname;
const baseUrl = environment.baseurl;

@Injectable({
  providedIn: 'root'
})
export class DownloadService {

    private _apiDownloadDocumento = baseUrl + brandName + '/DownloadDocumento?id='


    constructor(private http: HttpClient) { }

    getDocumento(id: string): Observable<any> {
        return this.http.get(this._apiDownloadDocumento + id,  { responseType: 'text' })
            .pipe(
                map((res: any) => {
                    return res;
                }),
                retry(1),
                catchError(this.handleError)
            );
    }

    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        return throwError(errorMessage);
    }


}
