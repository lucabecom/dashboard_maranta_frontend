import { TestBed, inject } from '@angular/core/testing';

import { GenericApiServiceService } from './generic-api-service.service';

describe('GenericApiServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GenericApiServiceService]
    });
  });

  it('should be created', inject([GenericApiServiceService], (service: GenericApiServiceService) => {
    expect(service).toBeTruthy();
  }));
});
