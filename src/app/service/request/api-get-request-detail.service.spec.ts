import { TestBed, inject } from '@angular/core/testing';

import { ApiGetRequestDetailService } from './api-get-request-detail.service';

describe('ApiGetRequestDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiGetRequestDetailService]
    });
  });

  it('should be created', inject([ApiGetRequestDetailService], (service: ApiGetRequestDetailService) => {
    expect(service).toBeTruthy();
  }));
});
