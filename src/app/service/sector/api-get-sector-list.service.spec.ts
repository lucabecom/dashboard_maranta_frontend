import { TestBed, inject } from '@angular/core/testing';

import { ApiGetSectorListService } from './api-get-sector-list.service';

describe('ApiGetSectorListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiGetSectorListService]
    });
  });

  it('should be created', inject([ApiGetSectorListService], (service: ApiGetSectorListService) => {
    expect(service).toBeTruthy();
  }));
});
