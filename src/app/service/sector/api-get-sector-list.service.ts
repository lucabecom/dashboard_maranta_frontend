import {Injectable} from '@angular/core';

import {Observable, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, retry, map} from 'rxjs/operators';
import {ApiLoginUtenteService} from '../utente/api-login-utente.service';

const brandName = environment.brandname;
const baseUrl = environment.baseurl;

@Injectable({
    providedIn: 'root'
})
export class ApiGetSectorListService {

    private _apiGetSectorList = baseUrl + brandName + '/Api/RequestsList/Sector';
    private _apiGetSectorListFiltered = baseUrl + brandName + '/Api/RequestsList/Sector/Filtered';

    // SessionId: "087dfd80-e1bb-4dfa-aa5f-036d0a7f12bd"

    private options: any;

    constructor(private http: HttpClient,
                private utente: ApiLoginUtenteService) {
    }

    getSectorList(sectorname: string): Observable<any> {
        this.options = {
            headers: new HttpHeaders()
                .set('SessionId', this.utente.getMySession())
        };
        return this.http.post(this._apiGetSectorList, {'SectorName': sectorname},
            this.options
        )
            .pipe(
                map((res: any) => {
                    return res;
                }),
                retry(1),
                catchError(this.handleError)
            );
    }

    getSectorListFiltered(modelview: any): Observable<any> {
        this.options = {
            headers: new HttpHeaders()
                .set('SessionId', this.utente.getMySession())
        };
        return this.http.post(this._apiGetSectorListFiltered, modelview,
            this.options
        )
            .pipe(
                map((res: any) => {
                    return res;
                }),
                retry(1),
                catchError(this.handleError)
            );
    }

    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        return throwError(errorMessage);
    }
}
