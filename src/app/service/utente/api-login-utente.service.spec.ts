import { TestBed, inject } from '@angular/core/testing';

import { ApiLoginUtenteService } from './api-login-utente.service';

describe('ApiLoginUtenteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiLoginUtenteService]
    });
  });

  it('should be created', inject([ApiLoginUtenteService], (service: ApiLoginUtenteService) => {
    expect(service).toBeTruthy();
  }));
});
