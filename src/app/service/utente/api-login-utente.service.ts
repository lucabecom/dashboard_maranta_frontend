import {Injectable} from '@angular/core';

import {Observable, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, retry, map} from 'rxjs/operators';

import 'rxjs/Rx';

const brandName = environment.brandname;
const baseUrl = environment.baseurl;


@Injectable({
    providedIn: 'root'
})
export class ApiLoginUtenteService {
    private mySessionId: string;
    private _apiURLLogin = baseUrl + brandName + '/App/ExternalLogin';
    // http://localhost:50878/Maranta/App/Login?username=admin@becomitalia.com&password=1234
    private _apiURLgetMyId = baseUrl + brandName + '/Session/GetMyId';
    private options: any;


    constructor(private http: HttpClient) {
    }

    login(username: string, password: string): Observable<any> {
        this.options = {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json; charset=utf-8')
        };
        return this.http.post(this._apiURLLogin, {
                'username': username,
                'password': password
            },
            this.options
        )
            .pipe(
                map((res: any) => {
                    this.mySessionId = res.SessionId;
                    localStorage.setItem(environment.brandname + '_SessionId', res.SessionId);
                    this.options = {
                        headers: new HttpHeaders()
                            .set('BrandName', brandName)
                            .set('Content-Type', 'application/json; charset=utf-8')
                            .set('withCredentials', 'true')
                            .set('SessionId', res.SessionId)
                    };
                    return res;
                }),
                retry(1),
                catchError(this.handleError)
            );
    }

    getMyId(): Observable<any> {
        this.options = {
            headers: new HttpHeaders()
                .set('BrandName', brandName)
                .set('Content-Type', 'application/json; charset=utf-8')
                .set('withCredentials', 'true')
                .set('SessionId', localStorage.getItem(environment.brandname + '_SessionId'))
        };
        return this.http.get(this._apiURLgetMyId, this.options)
            .pipe(
                map((response: any) => {
                    return <any>response;
                }),
                retry(1),
                catchError(this.handleError)
            );
    }

    getMySession() {
        if (localStorage.getItem(environment.brandname + '_SessionId')) {
            console.log('Session id: ', localStorage.getItem(environment.brandname + '_SessionId'));
            return localStorage.getItem(environment.brandname + '_SessionId');
        } else {
            return null;
        }

    }


    /*   getSession(username: string, password: string): Observable<any> {
           return this.http
               .get(this._apiURLgetSession, this.options)
               .map((response: Response) => {
                   return <any>response.json();
               })
               .catch(this.handleError);
       } */

    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        return throwError(errorMessage);
    }

}
